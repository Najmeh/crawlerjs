#! /bin/bash
# args: <total number of domains to crawl> <number of splits> < alexa filename3>
# example input: ./bootstrap.sh 500 10 ./mainCrawler-multiarg.py top-1m.csv 30

if [ "$#" -ne 5 ]; then
  echo "Arg: <total count> <split number> <python code> <Alexa file> <sleep intervals>"
  exit 1
fi

echo 'Start running the crawler!'
let B=$1/$2  #bucket size
echo 'Bucket size:'$B

if [ "$2" -ne 1 ]; then
        for i in $(seq 1 $(($2-1)) );
        do

                test="bucket"$i
                let Base=($i-1)*$B
                let End=($i*$B)
                echo 'base: '$Base
                echo 'end: '$End
                screen -dmS $test $3 "-a" $Base $End $4
                sleep $5

        done
fi

let Base=($2-1)*$B
echo 'final base'$Base
#screen -dmS "test"$2 ./pcode.py $Base $End $4
screen -dmS "bucket"$2 $3 "-a" $Base $1 $4

