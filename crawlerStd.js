
/*!
 * It is a crawling tool based on Casperjs.
 *
 * Author: Najmeh
 * Repository:    http://github.com/n1k0/casperjs
 *
 * Copyright (c) 2011-2012 Nicolas Perriault
//it extracts scripts and links.Then it follows the links found in each page.
//it sanitize the input for the iframes, it behaves for static behavior for example if the page dynamically add some urls or set a source for an iframe it will not catch that URLs.
//The question is how should we consider internal links? should we consider subdomains? They have different origin compared to the original website
//errors need to be debuged --> error are debuged for FB and twitter by casperjs --ignore-ssl=yes --ssl-protocol=any mycode.js 
//add some more error handeling to capture all the errors
 *
 * Part of source code is Copyright Joyent, Inc. and other Node contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */

//----------------------------------------- Initialization and global objects------------------------------	
var casper = require("casper").create({
    verbose: true,
//    logLevel: 'debug',
    pageSettings: {
      loadImages: true,
      loadPlugins: false,
      userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
    }
});
var MAX_LINKS_IN_PAGE =50;

var dataObj = {
	currentLink : "",
	title : "",
	stat: "",
	addedLinks : [], //links added to the queue to be visited later
	skippedLinks : [], //not a valid new local link
  	removedLinks : [], //the links removed because of exceeding MAX_num
	addedFrames : [],
	skippedFrames: [],
	scripts : [], 
	errors : [],
	messages : [],
	crawlerLogs:[] //logs of our crawler
}

// removing default options passed by the Python executable
//casper.cli.drop("cli");
//casper.cli.drop("casper-path");

if (casper.cli.args.length === 0 && Object.keys(casper.cli.options).length === 0) {
    casper.echo("No arg nor option passed").exit();
}
var target = casper.cli.args[0];
var domain = casper.cli.args[1];
var max_number = casper.cli.args[2];
casper.echo('Target: '+ target + "\n Max-num" + max_number);

var today = new Date(),
    yyyy = today.getFullYear(),
    mm = today.getMonth() + 1,
    d = today.getDate();

      if (mm < 10) {mm = '0' + mm;}
      if (d < 10) {d = '0' + d;}

      var filename =domain+ '-' +  yyyy + '-' + mm + '-' + d;


var fs = require('fs');
stream = fs.open(filename +'.json','aw');

//for error handeling
log = fs.open(filename +'.log', 'aw');


// The base links array
var links = [target];
var visitedLinks = [];//not neccessary, since links hold all the links including visited links

var max = parseInt(max_number);

// If we don't set a limit, it could go on forever
var upTo = max || 10;

var currentLink = 0;
Array.prototype.diff = function(a) {
			    return this.filter(function(i) {return a.indexOf(i) < 0;});	};


//-----------------------------------------Check Urls are well formed, local, bellow the MAX_NUM-------
function prepareLinks( found){


		//check for valid local links
		//need to be debuged with complicated urls and other websites
		var absLinks =Array.prototype.filter.call(found, function(e) {
			 var re = new RegExp("^https?\:\/\/([^/]+)(?:(?:\/.*)?|\/)", "i");

			var authority = re.exec(e);

			if (authority && !authority[1]){
    			log.writeLine('FATAL ERROR: Regex syntax error. For the link : ' + e + ' link is matched but no data could be extracted.\n ' + '\n Current Page is: '+ dataObj.currentLink);
			}
		//check if the authority is not null(cannot find a match). 
			return authority ? (authority[1].indexOf(domain)> -1) : false;
		    });
		
		//remove already visited links
		absLinks = Array.prototype.filter.call(absLinks, function(g){
				return  (visitedLinks.indexOf(g) < 0 && links.indexOf(g) < 0)?true : false;});
        
		//check for the number of links should be added to the list
		var linksNum = absLinks.length; 
		var result = [];
		if(linksNum > MAX_LINKS_IN_PAGE){
			this.echo('----------------------------MOre than 50 links found--------------------------');

    			log.writeLine('Info: More than MAX_LINKS found on the page:  ' +dataObj.currentLink);
			dataObj.crawlerLogs.push("Info: More than MAX_LINKS are found here");

			// it pushes at most  2*MAX_LINKS)INPAGE
			for(i= 0; i< linksNum; i = i + Math.floor(linksNum/MAX_LINKS_IN_PAGE) ){
				result.push(absLinks[i]);
			}
			
			dataObj.removedLinks = absLinks.diff(result);
		}else {
 	        	result = absLinks;
		 }
	return result;

}

//----------------------------------------- crawle the page------------------------------	
// Get the links, and add them to the links array
//add frames as a new link to follow and extract the scripts for it
function addLinks(link) {
    this.then(function() {


        var found = this.evaluate(searchLinks);
	dataObj.currentLink = link;

	this.echo('====================All the links: ----\n' + found );

//----------------------------------------- Page Links------------------------------	
	if(found){ //not necessary 
		
		var preparedLinks = prepareLinks.call(this, found);
		links = links.concat(preparedLinks);
		dataObj.addedLinks = preparedLinks;
		this.echo('\n==================Final links added: -----\n'+links);
		dataObj.skippedLinks = found.diff(preparedLinks).diff(dataObj.removedLinks);
		
	}


//----------------------------------------- Page Scripts------------------------------	
	var scripts = this.evaluate(extract_scripts);
	if(scripts){
		this.echo(scripts.length + " scripts found on " + link);
		this.echo("\n====================Scripts:--------\n"+ scripts.join("\n, "));
		dataObj.scripts = scripts;
	}


//----------------------------------------- Page Frame Links------------------------------	
        var framesFound = this.evaluate(searchFrames);
        if(framesFound){


		var validFrames = prepareLinks.call(this, framesFound);
		links = links.concat(validFrames);
		dataObj.addedFrames = validFrames;
		dataObj.skippedFrames = framesFound.diff(validFrames);
		this.echo('\n==================Final links added for the frames: -----\n'+ validFrames);

	}
//write back to the file after finishing crawling
  var data = JSON.stringify(dataObj, undefined, 2);
//sper.echo('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! write to data.json with url'+dataObj.currentLink);
  stream.writeLine(data);
  stream.flush();
  dataObj.title = "";
  dataObj.currentLink = "";
  dataObj.addedLinks = [];
  dataObj.skippedLinks = [];
  dataObj.removedLinks = []; //the links removed because of exceeding MAX_num
  dataObj.addedFrames = [];
  dataObj.skippedFrames = [];
  dataObj.scripts = [];
  dataObj.stat = "";
  dataObj.errors = [];
  dataObj.messages = [];
  dataObj.crawlerLogs = [];

    });
}

//seems both my qualifier and absolute uri are working _> so lets keep our code simple and use qualifier
function qualifyURL(url) {
    if(url){
    var a = document.createElement('a');
    a.href = url;
    return a.cloneNode(false).href;
}
else return url;
}


//----------------------------------------- Search for frames------------------------------	

function searchFrames(){

    var myFm = document.querySelectorAll('iframe');

    var fmSrc =  Array.prototype.map.call(myFm, function(e) {
		return e.src;
});
//we remove null or undefined values from the list
//return fmSrc.filter(function(e){return e}); 
    fmSrc = fmSrc.filter(function(e){return e}); 
    var qLinks = Array.prototype.map.call(fmSrc, function(url) {
		    if(url){
			    var a = document.createElement('a');
			    a.href = url;
			    return a.cloneNode(false).href;
			}	
		     else return url;
			}
	      		 );
	    return qLinks;
}


//----------------------------------------- Search for scripts------------------------------	
function extract_scripts(){

var my_scripts = document.querySelectorAll('script');
   var script_src =  Array.prototype.map.call(my_scripts, function(e) {
        return e.getAttribute('src');
});

return  script_src.filter(function(e){return e}); 
}


//----------------------------------------- Search for Links------------------------------	
// Fetch all links on the page and return them in absolute format

function searchLinks() {
var filter, map, hrefs;
    filter = Array.prototype.filter;
    map = Array.prototype.map;
    //query gets [] if it cannot find any and getAttribute get null if it dose not have attribute, so we need to filter them
    var as = document.querySelectorAll("a");
    if (as){//not neccessarry -> queryselect gets nodelist of size 0 if it cannot find any
	    var b = map.call(as, function(b){return b.getAttribute("href");});
	    //remove null hrefs in b array
	    hrefs =  filter.call(b, function(a) {return a;});
	    //suppose hrefs is  always an array(can have len 0)
	    //make all the urls in hrefs absolute addrsses
	    var qLinks = Array.prototype.map.call(hrefs, function(url) {
		    if(url){
			    var a = document.createElement('a');
			    a.href = url;
			    return a.cloneNode(false).href;
			}	
		     else return url;
			}
	      		 );
	    return qLinks;
	} 
    else return [];
}


//----------------------------------------- opens the page------------------------------	
// Just opens the page and prints the title

function start(link) {
    
    this.echo('lets try the link:--  '+ link + ' -------------');

    this.start(link, function() {
        this.echo('Page title: ' + this.getTitle());
	//there is a bug in phantom that always give 200 for error codes!!!
	var stat = this.status().currentHTTPStatus;
	dataObj.stat = stat;
	dataObj.title = this.getTitle();
        this.echo('\nPage Status ' + stat);
	visitedLinks.push(link);
	
//	this.echo('\nvisited Links start: ====================='+ visitedLinks.length);
    });
this.echo('OK, it is loaded\n');
//it shows the line before the title
}

//----------------------------------------- starts the crawler------------------------------	
// As long as it has a next link, and is under the maximum limit, will keep running

function check() {
	this.echo('\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    if (links[currentLink] && currentLink < upTo) {
        this.echo('--- Link ' + currentLink + ' ---');
        start.call(this, links[currentLink]);
        addLinks.call(this, links[currentLink]);
        currentLink++;
        this.run(check);
    } else {
        this.echo("All done.");
	stream.close();
	log.close();
        this.exit();
    
}
}

casper.start().then(function() {
    this.echo("Starting");
});

casper.run(check);

//----------------------------------------- error handeling------------------------------	
/*  
  onError: not null,
  onLoadError: null,
  onPageInitialized: null,
  onResourceReceived: not null,
  exitOnError: true,
  httpStatusHandlers: { },

*/
  // if console error exists
//Emitted when retrieved page leaves a Javascript error uncaught:
  casper.on('page.error', function(msg, trace) {
    var error = {
      msg: msg,
      file: trace[0].file,
      line: trace[0].line,
      func: trace[0]['function']
    };

    log.writeLine('ERROR: ' + error.msg + '\n on Page: '+ dataObj.title);
    log.writeLine('file: ' + error.file);
    log.writeLine('line: ' + error.line);
    log.writeLine('function: ' + error.func);
    log.flush();
    dataObj.errors.push(error);
    casper.echo('Page error :'+ msg, 'ERROR');

  });
  //to catch resource errors
//CasperJS just forwards the native PhantomJS event handler onResourceReceived
/*

    id : the number of the requested resource
    url : the URL of the requested resource
    time : Date object containing the date of the response
    headers : list of http headers
    bodySize : size of the received content decompressed (entire content or chunk content)
    contentType : the content type if specified
    redirectURL : if there is a redirection, the redirected URL
    stage : "start", "end" (FIXME: other value for intermediate chunk?)
    status : http status code. ex: 200
    statusText : http status text. ex: OK

*/
//it works and writes into the file and console but not dataOBJ
casper.on('resource.received', function(resource) {
    var status = resource.status;
    if(
//	resource.stage === "end" &&
					status >= 400) {
	var error = 'Resource ' + resource.url + ' failed to load (' + status + ')';
	dataObj.errors.push(error);
	this.echo('--- Link err ' + currentLink + ' ---', {fg:'blue', bold:true});
	casper.echo(this.colorizer.format(error, {fg: 'red', bold: true}) );
//        casper.echo(error, 'ERROR');
        log.writeLine(error);
	log.flush();
//dataObj is already written to the file so there is no way to write it to the same object
    }
});

// if console message exists
//everytime a message is sent to the client-side console
//catch site console.log 
  casper.on('remote.message', function(msg) {
    log.writeLine('MESSAGE: ' + msg);
    log.flush();
    dataObj.messages.push(msg);
    casper.echo('Remote Msg :'+ msg, 'ERROR');
  });

  // stop crawl if there's an internal error
  casper.on('error', function(msg, backtrace) {
    log.writeLine('INTERNAL ERROR: ' + msg + 'on '+ dataObj.title );
    log.writeLine('BACKTRACE:' + backtrace);
    log.flush();
    log.close();
    casper.echo('Internal error :'+ msg, 'ERROR');
    this.die('Crawl stopped because of errors.');
  });
//----------------------------------------------extra error handlers
//Emitted when a complete callback has errored.
casper.on('complete.error', function(err) {
	this.echo("COmplete.error", "ERROR");
    this.die("Complete callback has failed: " + err);
});

//Emitted when the Casper.die() method has been called.
casper.on('die', function(err) {
	this.echo("die error", "ERROR");
});
/*
casper.on('http.status.404', function(resource) {
    casper.echo(resource.url + ' is 404'), "ERROR");
});
*/
//Emitted when any requested resource fails to load properly.
//it is fired when any resource (link, image(if enabled),...) can not be loaded... 
casper.on("resource.error", function(resourceError){
    
 //   this.echo("resource error ", "ERROR");
    var error = 'Unable to load resource (#' + resourceError.id + 'URL:' + resourceError.url + ')' +
    		'Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString;
//	console.log('Unable to load resource (#' + resourceError.id + 'URL:' + resourceError.url + ')');
 //   console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
	dataObj.errors.push(error);
//        casper.echo(error, 'ERROR')
casper.echo(this.colorizer.format(error, {fg: 'red', bold: true}) );
        log.writeLine(error);
	log.flush();
});

//Emitted when a step function has errored.
casper.on('step.error', function(err) {
    this.echo('Step Error', "ERROR");
    this.die("Step has failed: " + err);
});


//Emitted when the Casper.log() method has been called. 
/*casper.on('log', function(err) {
    this.echo("log error ", "ERROR");
});
*/
//Emitted when PhantomJS’ WebPage.onLoadFinished event callback has been called and failed.
//'Cannot open ' + resource.url, 'error'
casper.on('load.failed', function(err) {
   this.echo('load Fail', 'ERROR'); 
   this.die("Complete callback has failed: " + err);
});


  
