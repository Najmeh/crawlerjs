/*!
 * It is a crawling tool based on Casperjs.
 *
 * Author: Najmeh
 * Repository: https://bitbucket.org/Najmeh/crawlerjs   
 *
 * The crawler extracts scripts and links.Then it recursively visits the links found in each page or frame.
 * Inputs to the scripts are: target URL(ex. http://google.com), base domain(ex. google.com), and the number of links we would like to visit.
 * 
 * Features: 
 * It extracts local links (links that contain the specified base domain as a part of their authority) of a target.
 * For each visited URL, it records the local URL's found in it, the scripts included and the URLs of the frames in the page.The data will be 
 * stored to a json file in the same directory. 
 * It keeps tracks of errors,logs and messages both in a log file and in the data file mentioned above. 
 * In order to speed up the crawling process, loading of images and plugins is disabled.
 * In orser to increase the speed of the tool, if the number of links in a page exceeds a predefined threshold(MAX_LINKS_IN_PAGE), it picks up 
 * at most twice of the threshold from the links found in that page.
 * It has handlers for console.log, resource.received,page.error, casperjs internal errors and step errors.
 * To ignore ssl errors, we should run it with the following options: " casperjs --ignore-ssl=yes --ssl-protocol=any mycode.js <input>"
 * 
 * Need to be improved:
 * It dose not find dynamic scripts added after loading the page.
 * Add extracting frame sets.
 * Adding event listeners for handeling syntax errors of included js.
 */

//----------------------------------------- Initialization and global objects------------------------------	

WAIT_TO = 1000;
var casper = require("casper").create({
    verbose: true,
//    logLevel: 'debug',
    pageSettings: {
      loadImages: false,
      loadPlugins: false,
      userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
    }
});

//Threshold of max link in each page
var MAX_LINKS_IN_PAGE =50;
var fs = require('fs');

var dataObj = {
	currentLink : "",
	title : "",
	stat: "",
	addedLinks : [],	 //Links added to the queue to be visited later
	skippedLinks : [], 	 //Not valid new local links
  	removedLinks : [],	 //Links removed because of exceeding MAX_num
	addedFrames : [],
	skippedFrames: [],
	scripts : [], 		 //All the scripts found in the page
	queueList : [],    	//The links in the waiting queue at the time of visiting this page
	errors : [],
	messages : [],
	crawlerLogs:[] 		 //Internal log of the crawler
}

// removing default options 
//casper.cli.drop("cli");
//casper.cli.drop("casper-path");

//Receiving user inputs
if (casper.cli.args.length === 0 && Object.keys(casper.cli.options).length === 0) {
    casper.echo("No arg nor option passed").exit();
}

/////////////////////////////////////////////
var input = casper.cli.raw.get(0);
var output = casper.cli.raw.get(1);
var domain = casper.cli.raw.get(2);
var max_number = casper.cli.raw.get(3);
casper.echo("\n--------------------------input file name"+ input);


casper.echo("\n--------------------------output name:  "+ output );

input = JSON.parse(fs.read(input));
log = fs.open(output +'.log', 'a');
stream = fs.open(output +'.json','a');

casper.echo("\n--------------------------input file content "+ input);

var links = input;
var visitedLinks = [];

//set the max number of links should be visited all together
var max = parseInt(max_number);

// If we don't set a limit, it could go on forever
var upTo = max || 10;

var currentLink = 0;

//the difference of two arrays
Array.prototype.diff = function(a) {
			    return this.filter(function(i) {return a.indexOf(i) < 0;});	};


//----------------------------------------Check URLs are well formed, local, less than MAX_NUM-------

function prepareLinks(found){

	//check for valid local links
	var absLinks =Array.prototype.filter.call(found, function(e) {
		var re = new RegExp("^https?\:\/\/([^/]+)(?:(?:\/.*)?|\/)", "i");
		var authority = re.exec(e);
		if (authority && !authority[1]){
			var err  = 'FATAL ERROR: Regex syntax error. For the link : ' + e + ' link is matched but no data could be extracted. Current Page is: '+ dataObj.currentLink;
			log.writeLine(err);
			dataObj.crawlerLogs.push(err);
			return false;
		}
		//check if the authority is not null(cannot find a match). 
		return authority ? (authority[1].indexOf(domain)> -1) : false;
		});
		
		//remove already visited links
		absLinks = Array.prototype.filter.call(absLinks, function(g){
				return  (visitedLinks.indexOf(g) < 0 &&  links.indexOf(g) < 0)? true : false;});
        
		//check for the number of links that are being add to the queue
		var linksNum = absLinks.length; 
		var result = [];
		if(linksNum > MAX_LINKS_IN_PAGE){
			this.echo('----------------------------MOre than 50 links found--------------------------');
    			log.writeLine('Info: More than MAX_LINKS found on the page:  ' +dataObj.currentLink);
			dataObj.crawlerLogs.push("Info: More than MAX_LINKS is found.");

			// it pushes at most  2*MAX_LINKS_IN_PAGE
			for(i= 0; i< linksNum; i = i + Math.floor(linksNum/MAX_LINKS_IN_PAGE) ){
				result.push(absLinks[i]);
			}
			
			dataObj.removedLinks = absLinks.diff(result);
		}else {
 	        	result = absLinks;
		 }
	return result;
}

//----------------------------------------- crawl the page------------------------------	
// It finds the links, and adds them to the links array
//add frames as a new link to follow and extract the scripts for it

function addLinks(link) {
    this.then(function() {

        var found = this.evaluate(searchLinks);
	dataObj.currentLink = link;

//	this.echo('====================All the links: ----\n' + found );

//----------------------------------------- Page Links------------------------------	
	if(found){ //not necessary 
		
		var preparedLinks = prepareLinks.call(this, found);
		links = links.concat(preparedLinks);
		dataObj.addedLinks = preparedLinks;
		dataObj.skippedLinks = found.diff(preparedLinks).diff(dataObj.removedLinks);
	//	this.echo('\n==================Final links added: -----\n'+links);
		
	}

//----------------------------------------- Page Scripts------------------------------	
	var scripts = this.evaluate(extract_scripts);
	if(scripts){
		this.echo(scripts.length + " scripts found on " + link);
		this.echo("\n====================Scripts:--------\n"+ scripts.join("\n, "));
		dataObj.scripts = scripts;
	}


//----------------------------------------- Page Frame Links------------------------------	
        var framesFound = this.evaluate(searchFrames);
        if(framesFound){
		var validFrames = prepareLinks.call(this, framesFound);
		links = links.concat(validFrames);
		dataObj.addedFrames = validFrames;
		dataObj.skippedFrames = framesFound.diff(validFrames);
//		this.echo('\n==================Final links added for the frames: -----\n'+ validFrames);
	}
//add links in the queue
	dataObj.queueList = links.slice(currentLink, links.length);
//	this.echo("=========================== queue Links "+ dataObj.queueList);

//write back to the file after finishing crawling
	var data = JSON.stringify(dataObj, undefined, 2);
	stream.writeLine(data);
	stream.flush();
	dataObj.title = "";
	dataObj.currentLink = "";
	dataObj.addedLinks = [];
	dataObj.skippedLinks = [];
	dataObj.removedLinks = []; 
	dataObj.addedFrames = [];
	dataObj.skippedFrames = [];
	dataObj.scripts = [];
	dataObj.stat = "";
	dataObj.errors = [];
	dataObj.messages = [];
	dataObj.crawlerLogs = [];
    });
}

//It converts relative addresses to absolute addresses(complex regex evaluation fails for some cases, that's why I use this simple code here!)
function qualifyURL(url) {
	if(url){
		var a = document.createElement('a');
		a.href = url;
		return a.cloneNode(false).href;
	}
	else return url;
}


//----------------------------------------- Search for frames------------------------------	

function searchFrames(){

	var myFm = document.querySelectorAll('iframe');
	var fmSrc =  Array.prototype.map.call(myFm, function(e) {
		return e.src;
		});
//remove null or undefined values from the list
	fmSrc = fmSrc.filter(function(e){return e});
	var qLinks = Array.prototype.map.call(fmSrc, function(url) {
		if(url){
			var a = document.createElement('a');
			a.href = url;
			return a.cloneNode(false).href;
			}	
		else return url;
		}
	      	);
	return qLinks;
}
 

//---------------------convert relative addresses to absolute addresses--------------------
function qualifyURL(url) {
	var qLinks = Array.prototype.map.call(fmSrc, function(url) {
		if(url){
			var a = document.createElement('a');
			a.href = url;
			return a.cloneNode(false).href;
		}	
		else return url;
		}
	      	);
	return qLinks;
}

//----------------------------------------- Search for scripts------------------------------	

function extract_scripts(){

	var my_scripts = document.querySelectorAll('script');
	var script_src =  Array.prototype.map.call(my_scripts, function(e) {
		return e.getAttribute('src');
		});
	return  script_src.filter(function(e){return e}); 
}


//----------------------------------------- Search for Links------------------------------	
// Fetch all the links on the page and return them as absolute addresses

function searchLinks() {
	var filter, map, hrefs;
	filter = Array.prototype.filter;
	map = Array.prototype.map;
//query gets [] if it cannot find any and getAttribute get null if it dose not have attribute, so we need to filter them
	var as = document.querySelectorAll("a");
	if (as){//not neccessarry -> queryselect gets nodelist of size 0 if it cannot find any
		var b = map.call(as, function(b){return b.getAttribute("href");});
	    //remove null hrefs in b array
		 hrefs =  filter.call(b, function(a) {return a;});
	    //suppose hrefs is  always an array(can have len 0)
	    //make all the urls in hrefs absolute addresses
		var qLinks = Array.prototype.map.call(hrefs, function(url) {
			if(url){
				var a = document.createElement('a');
				a.href = url;
				return a.cloneNode(false).href;
			}	
			else return url;
			}
	      		 );
	return qLinks;
	} 
	else return [];
}


//----------------------------------------- opening the page------------------------------	
//Opens the page and prints the title

function start(link) {
	this.echo('lets try the link:--  '+ link + ' -------------');
	this.start(link, function() {
        this.echo('Page title: ' + this.getTitle());
	//there is a bug in phantom so that it always gives 200 as the status code!!!
	var stat = this.status().currentHTTPStatus;
	dataObj.stat = stat;
	dataObj.title = this.getTitle();
        this.echo('\nPage Status ' + stat);
	visitedLinks.push(link);
	});
//waits to load the page
	this.then(function(){
		casper.wait(WAIT_TO, function() {
	    this.echo("I've waited for a couple of seconds.");
	}); });
	
	this.echo('OK, it is loaded\n');
//it shows the line before the title
}

//----------------------------------------- starting the crawler------------------------------	
// As long as it has a next link, and is under the maximum limit, it will keep running

function check() {
	this.echo('\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
	if (links[currentLink] && currentLink < upTo) {
		curLink = links[currentLink];
		this.echo('--- Link ' + currentLink + ' ---');
		currentLink++;
		start.call(this, curLink);
		addLinks.call(this, curLink);
		this.run(check);
	} else {
		this.echo("All done.");
		stream.close();
		log.close();
		this.exit();
	}
}

casper.start().then(function() {
	this.echo("Starting");
});

casper.run(check);

//----------------------------------------- error handeling------------------------------	
/*  
  onError: not null,
  onLoadError: null,
  onPageInitialized: null,
  onResourceReceived: not null,
  exitOnError: true,
  httpStatusHandlers: { },

*/

// if console error exists
//Emitted when retrieved page leaves a Javascript error uncaught:
casper.on('page.error', function(msg, trace) {
	var error = {
		msg: msg,
		file: trace[0].file,
		line: trace[0].line,
		func: trace[0]['function']
	};

	log.writeLine('ERROR: ' + error.msg + '\n on Page: '+ dataObj.title);
	log.writeLine('file: ' + error.file);
	log.writeLine('line: ' + error.line);
	log.writeLine('function: ' + error.func);
	log.flush();
	dataObj.errors.push(error);
	casper.echo('Page error :'+ msg, 'ERROR');
});

//to catch resource errors
//CasperJS just forwards the native PhantomJS event handler onResourceReceived
/*
	
    id : the number of the requested resource
    url : the URL of the requested resource
    time : Date object containing the date of the response
    headers : list of http headers
    bodySize : size of the received content decompressed (entire content or chunk content)
    contentType : the content type if specified
    redirectURL : if there is a redirection, the redirected URL
    stage : "start", "end" (FIXME: other value for intermediate chunk?)
    status : http status code. ex: 200
    statusText : http status text. ex: OK

*/
//it works and writes in the log file and console but not dose not log it in dataObj!!
casper.on('resource.received', function(resource) {
	var status = resource.status;
	if(
//	resource.stage === "end" &&
		status >= 400) {
		var error = 'Resource ' + resource.url + ' failed to load (' + status + ')';
		dataObj.errors.push(error);
//		this.echo('--- Link err ' + currentLink + ' ---', {fg:'blue', bold:true});
//		casper.echo(this.colorizer.format(error, {fg: 'red', bold: true}) );
		log.writeLine(error);
		log.flush();
	}
});

// if console message exists,catches site console.log 
//everytime a message is sent to the client-side console
casper.on('remote.message', function(msg) {
	log.writeLine('MESSAGE: ' + msg);
	log.flush();
	dataObj.messages.push(msg);
//	casper.echo('Remote Msg :'+ msg, 'ERROR');
});

// stop crawl if there's an internal error
casper.on('error', function(msg, backtrace) {
	log.writeLine('INTERNAL ERROR: ' + msg + 'on '+ dataObj.title );
	log.writeLine('BACKTRACE:' + backtrace);
	log.flush();
	log.close();
//	casper.echo('Internal error :'+ msg, 'ERROR');
	this.die('Crawl stopped because of errors.');
});

//----------------------------------------------some more error handlers

//Emitted when a complete callback has errored.
casper.on('complete.error', function(err) {
	this.echo("COmplete.error", "ERROR");
    this.die("Complete callback has failed: " + err);
});

//Emitted when the Casper.die() method has been called.
casper.on('die', function(err) {
	this.echo("die error", "ERROR");
});

/*
casper.on('http.status.404', function(resource) {
    casper.echo(resource.url + ' is 404'), "ERROR");
});
*/

//It will be fired when any resource (link, image(if enabled),...) fails to load
casper.on("resource.error", function(resourceError){
    
	var error = 'Unable to load resource (#' + resourceError.id + 'URL:' + resourceError.url + ')' +
    			'Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString;
	dataObj.errors.push(error);
//	casper.echo(this.colorizer.format(error, {fg: 'red', bold: true}) );
        log.writeLine(error);
	log.flush();
});

//Emitted when a step function has errored.
casper.on('step.error', function(err) {
	this.echo('Step Error', "ERROR");
	this.die("Step has failed: " + err);
});


//Emitted when the Casper.log() method has been called. 
/*casper.on('log', function(err) {
    this.echo("log error ", "ERROR");
});
*/

//Emitted when PhantomJS’ WebPage.onLoadFinished event callback has been called and failed.
casper.on('load.failed', function(err) {
	this.echo('load Fail', 'ERROR'); 
	this.die("Complete callback has failed: " + err);
});


  
