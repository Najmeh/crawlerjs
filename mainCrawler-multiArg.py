#!/usr/bin/env python3

'''
A wrapper for searching Bing and running Casperjs.
input : <beginning of Alexa list> <end element of Alexa List> <Alexa file path> 
Sends requests to Bing Search API to extract popular links of a domain, 
Visits links of a domain provided by Bing,
Can crawl top n domains of Alexa list using the same strategy
Crawl a specific domain
'''
import runCasper
import csv
import sys, getopt
import json
import configparser
import random

#-------Initialization--------------------------------------

config = configparser.ConfigParser()
config.readfp(open(r'crawler.config'))
ALEXA_FILE = config.get('Main', 'ALEXA_FILE')

NUM_LINKS_TO_VISIT = config.get('Main', 'NUM_LINKS_TO_VISIT')
#-------Helper functions--------------------------------------
''' 
args: -a <base>  <end>  <Alexa filename>

'''
def crawlAlexa(arg):
	base = int(arg[0])
	end = int(arg[1])
	filename = arg[2]
	alexaF = open(filename, 'r')
	reader = list(csv.reader(alexaF))
	for i in range(base, end):
		target = reader[i][1]
		print("|||||||||||||||||||||||||||||||||||||Start Crawling" + target +'||||||||||||||||||||||||||||||||||||||||||\n')
		crawlDomain(target)	
	alexaF.close()
		
#------------------------------ rand Alexa-------------------------------
def randAlexa(n):
	print("---------------------> " +n)
	rnums = []
	for i in range(int(n)):
		rnums.append(random.randrange(0, 999999))
	
	alexaF = open(ALEXA_FILE, 'r')
	reader = list(csv.reader(alexaF))
	for r in rnums:
		target = reader[r][1]
		print("|||||||||||||||||||||||||||||||||||||Start Crawling" + target +'||||||||||||||||||||||||||||||||||||||||||\n')
		crawlDomain(target)	
	alexaF.close()

#-------------crawl a specific domain-----------------
def crawlDomain(domain):
	print("in crawler domain :  " + domain)
	runCasper.run("http://"+domain ,domain, NUM_LINKS_TO_VISIT)

#------------------main-------------------------------

def main(argv):
		if len(argv) < 1:
			print ('crawl.py -a<start of Alexa list> <end of Alexa list> <Alexa filename>  -c<domain> -l<randAlexa>')
			sys.exit(2)
		opt = argv[0]
		if opt == '-a':
			crawlAlexa(argv[1:])
		elif opt =="-c":
			crawlDomain(argv[1:])
		elif opt =="-l":
			randAlexa(arg[1:])


if __name__ == "__main__":
   main(sys.argv[1:])
