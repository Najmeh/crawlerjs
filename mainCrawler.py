#!/usr/bin/env python3

'''
A wrapper for searching Bing and running Casperjs.

Sends requests to Bing Search API to extract popular links of a domain, 
Visits links of a domain provided by Bing,
Can crawl top n domains of Alexa list using the same strategy
Crawl a specific domain
'''
import runCasper
import csv
import sys, getopt
import json
import configparser

#-------Initialization--------------------------------------

config = configparser.ConfigParser()
config.readfp(open(r'crawler.config'))
ALEXA_FILE = config.get('Main', 'ALEXA_FILE')

NUM_LINKS_TO_VISIT = config.get('Main', 'NUM_LINKS_TO_VISIT')
#-------Helper functions--------------------------------------

def crawlAlexa(n):
	alexaF = open(ALEXA_FILE, 'r')
	reader = list(csv.reader(alexaF))
	for i in range(int(n)):
		target = reader[i][1]
		print("|||||||||||||||||||||||||||||||||||||Start Crawling" + target +'||||||||||||||||||||||||||||||||||||||||||\n')
		crawlDomain(target)	
	alexaF.close()
		
def crawlDomain(domain):
	print("in crawler domain :  " + domain)
	runCasper.run("http://"+domain ,domain, NUM_LINKS_TO_VISIT)

def main(argv):
	try:
		opts, args = getopt.getopt(argv,"ha:c:",[ "alexa=", "crawl=" ])
	except getopt.GetoptError:
		print ('crawl.py -a<topAlexa> -c<domain>')
		print ('-a:Crawl n top list of Alexa, -c:Crawl a specific domain')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print ('crawl.py -a<topAlexa> -c<domain>')
			sys.exit()
		elif opt in ("-a", "--alexa"):
			crawlAlexa(arg)
		elif opt in ("-c", "--crawl"):
			crawlDomain(arg)

if __name__ == "__main__":
   main(sys.argv[1:])
