#!/usr/bin/env python3

""""
Extracts Bing's Results and calls Casper to scrape the pages. 
arguments : Bing results file
Handles Casper crashes and Timeout.

'''

'''A temporary file for passing inputs is used. It is better to explicitly dump JSON into a file than trying to parse the console messages from casperjs as they can be interleaved with debug strings and such.'''
"""

import tempfile
import json
import os
import subprocess
import sys
from urllib.parse import urlparse
import datetime
import configparser

#-------Initialization--------------------------------------

config = configparser.ConfigParser()
config.readfp(open(r'crawler.config'))
TIMEOUT = int(config.get('Run Casperjs', 'TIMEOUT'))
NUM_of_CALL = int(config.get('Run Casperjs', 'NUM_of_CALL'))
JS_SCRIPT = config.get('Run Casperjs', 'JS_SCRIPT')
CASPER = config.get('Run Casperjs', 'CASPER')
SSL_IGNORE = config.get('Run Casperjs', 'SSL_IGNORE')
SSL_ANY = config.get('Run Casperjs', 'SSL_ANY')

APP_ROOT = os.path.dirname(os.path.realpath(__file__))
SCRIPT = os.path.join(APP_ROOT,JS_SCRIPT)

#-------Some Helper functions--------------------------
def log(msg):
	with open('main.log', 'a+') as f :
		f.write(msg+"\n") 
	#pass

def getFileName(target):
	return target+ '-' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

def getURL(u):
        nl = urlparse(u).netloc
        return nl if len(nl)>=0 else 'rel: '+u

#-------parse casper results--------------
#what if it is not complete?!! we should sanitize it first
def parseJsonFile(fileName):
	jsonFile = open(fileName, 'r')
	jsonData = jsonFile.read()
	jsonData = '[' + jsonData + ']';
	jsonData = jsonData.replace('}\n{', '},\n{');
	jsonObjs = json.loads(jsonData)
	return jsonObjs
#-------Extracts scripts from casperi results(json file)--------------
def extractScripts(fileName):
	jsonObjs = parseJsonFile(fileName)
	scripts = []
	for jsonObj in jsonObjs:
		scripts.extend(jsonObj['scripts'])
#	scripts = [s.encode('ascii') for s in scripts]
# it will not miss non http:// addresses using getURL
#### dose it miss internl scipts, how it removes null and empty results fromgetUrl
#### we can then have a separate module for analysing scripts!!!,
##let's have it now to see what is happening

	dijScripts = [getURL(s)  for s in scripts]
	dijScripts =  (list(set(dijScripts)))
	print (str(dijScripts))
	with open(fileName.split('.')[0]+'.script', 'w') as outF:
		outF.write(json.dumps(dijScripts))
	return dijScripts

#-------Prepare input file to call casper--------------
def _makeInputFile(Urls):
	inputF = tempfile.NamedTemporaryFile(mode="w", delete=False)
	inputJSON = json.dumps(Urls)
	inputF.file.write(inputJSON)
	# we close the temporary input file because casperjs wants to read form it.
	inputF.file.close()
	return inputF.name

#--------------------------------call casper-------------
# Error handeling for return code and timeout are added
# It works with python3, timeout in subprocess call is not supported in python 2
#ps aux | grep casperjs shows that ssl options are really passed, so it should work.
	# 	For both check_output and check_call: 
#	If the return code is non-zero it raises a CalledProcessError. The CalledProcessError object will have the return code in the returncode attribute. 
#	We used check_call to see the result of running casperjs, because using check_output, stdout is used internally(The check_output function returns a bit strnig of outputs but check_call returns the process return code)

def run(link, domain, n): # link argument  should be a complete url (like http://google.com)
	print("In run function, target is : " + domain)
	log("\n\n"+datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ " Running Casper for " + link)

	# check for validation of domain later!!
	#target = "http://"+domain
	OUTPUT_FILE = getFileName(domain) 
	log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ " Output file is: " + OUTPUT_FILE)
	
	Urls = [link]
	count = NUM_of_CALL
	visited = 0
	INPUT_FILE = ""
	while count >= 0:

		if INPUT_FILE != "":
			os.remove(INPUT_FILE)	#remove already created input files

		INPUT_FILE = _makeInputFile(Urls)
			
		print ([CASPER, SSL_IGNORE, SSL_ANY, SCRIPT, INPUT_FILE, OUTPUT_FILE, domain, str(int(n)-visited)])
		params = [CASPER, SSL_IGNORE, SSL_ANY, SCRIPT, INPUT_FILE, OUTPUT_FILE, domain, str(int(n)-visited)]
		log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ "\tIteration " + str(NUM_of_CALL - count) + " started." )
		try: 
			count -= 1
			print (subprocess.check_call(params, stderr=subprocess.STDOUT , timeout=TIMEOUT))

		#The TimeoutExpired exception will be re-raised after the child process has terminated.
		except subprocess.TimeoutExpired as ee:
			print ("Timeout")
			log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ "\t\tTimeout ")

		except subprocess.CalledProcessError as e:
			
			err = "Error: " + str(e.returncode)  
			print ( err )
			log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ "\t\tTimeout ")
		except :	
			err = "An Error occured while running Casperjs!"
			print (err)
			log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ "\t\t"+err)
		# If something bad happened, we skip bad url and continue
		pageObjs = parseJsonFile(OUTPUT_FILE+".json")
		visited = len(pageObjs)
		
		#If it couldn't visit any links, we ignore it altogether(Maybe I change it in future)
		if (visited < 1):
			err = "FATAL ERROR: The result of running Casperjs is empty! "
			print(err)
			log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ "\t\t\t"+err)
			break

		#Start a new iteration of running casper if there is anything to visit
		elif (visited < int(n)):
			print('////////////////////////////////////// Start a new iteration //////////////////////////////')
			urlsInQueue = pageObjs[-1]["queueList"]
			if (len(urlsInQueue)== 0):
				print ("------------------- No link found in the queue---------------------\n")
				log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ "\t\t\tNo link to visit for the next iteration!")
				break
			else :
				Urls = urlsInQueue[1:]   # remove first  element of the queue that causes the crach
				print("\n------------------------ missed URL: "+ urlsInQueue[0] +"--------------------\n" )
				log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ "\t\t\t A url is skipped and continued with the rest of links in the queue\n\t\t\t\t\t URL: "+ urlsInQueue[0])
		else:
			print ("\n---------------------Successfully Finished crawling all " + n + " links!!--------------------")
			log(datetime.datetime.now().strftime("%Yi%m%d-%H%M%S")+ "\t\t\t succesfully crawled "+ str(visited) + " Links")
			break

#it does not remove it automatically as we set delete=False
	os.remove(INPUT_FILE)
#	inputF = None


